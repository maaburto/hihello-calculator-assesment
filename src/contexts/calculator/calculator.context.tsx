import React, { createContext, FC, useContext, useMemo } from "react";
import { CALCULATE, GATHER_OPERATIONS } from "./calculator.actions";
import calculatorReducer from './calculator.reducers';

interface IContextCalculatorProps {
  children: React.ReactNode;
}


export interface CalculatorState {
  inCalculation: string;
  result: number;
}

export interface ICalculatorContext extends CalculatorState {
  calculate: () => void;
  gatherOperations: (inCalculation: string) => void;
}

export type Action =
  | { type: 'CALCULATE' }
  | { type: 'GATHER_OPERATIONS'; inCalculation: string };


export const CalculatorContext = createContext<ICalculatorContext>({
  inCalculation: '',
  result: 0,
  calculate: () => { },
  gatherOperations: () => { },
});

CalculatorContext.displayName = 'CalculatorContext'

export const CalculatorProvider = ({ children }: IContextCalculatorProps) => {
  const [state, dispatch] = React.useReducer(calculatorReducer, {
    inCalculation: '',
    result: 0,
  })

  const calculate = () => dispatch({ type: CALCULATE });
  const gatherOperations = (inCalculation: string) => dispatch({ type: GATHER_OPERATIONS, inCalculation })

  const value = useMemo(
    () => ({
      ...state,
      calculate,
      gatherOperations,
    }),
    [state]
  )

  return <CalculatorContext.Provider value={value}>
    {children}
  </CalculatorContext.Provider>
}

export const useCalculatorContext = () => useContext(CalculatorContext);
