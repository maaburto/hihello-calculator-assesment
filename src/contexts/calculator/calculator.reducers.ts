import { calculateNumberFromString } from "../../utils/calculation-numbers";
import { CALCULATE, GATHER_OPERATIONS } from "./calculator.actions";
import { Action, CalculatorState } from "./calculator.context";

type Reducer = (prevState: CalculatorState, action: Action) => CalculatorState;

const reducer: Reducer = (state, action) => {
  switch (action.type) {
    case CALCULATE:
      return {
        ...state,
        result: calculateNumberFromString(state.inCalculation),
      };

    case GATHER_OPERATIONS: {
      return {
        ...state,
        inCalculation: `${state.inCalculation}${action.inCalculation}`.trim(),
      };
    }
  }
};

export default reducer;
