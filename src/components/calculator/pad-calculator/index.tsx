import { FC, useEffect, useState } from "react";
import { useCalculatorContext } from "../../../contexts/calculator/calculator.context";


const PadCalculator: FC = () => {
  const [displayToPad, setDisplayToPad] = useState('0');
  const { inCalculation, result } = useCalculatorContext();

  useEffect(() => {
    if (result !== 0) {
      setDisplayToPad(result.toString());
      return;
    }
    
    if (inCalculation) {
      setDisplayToPad(inCalculation);
    }

  }, [inCalculation, result])

  return (
    <div className="flex flex-row justify-end">
      <span className="text-5xl font-semibold py-10 pr-4">
        {displayToPad}
      </span>
    </div>
  );
}


export default PadCalculator;
